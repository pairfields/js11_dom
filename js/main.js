"use strict";

//▸ Используя полученные знания из видеоуроков примените каждый метод хотя бы раз в вашем текущем приложении

// для удобства заполнено изначальными значениями
let personalMovieDB = {
  count: 50,
  movies: {logan:8.2,
					wall_e:10
				},
  actors: {},
  genres: ['drama', 'action', 'comedy'],
  private: false,

  // метод limit проверяет чтоб строка не была пустой, null или больше 50 символов
  // и отдает соответствующее булевое значение
  limit: function (string) {
    if (string == "" || string == null || string.length > 50) {
      return true;
    } else {
      return false;
    }
  },

  // метод kinoman проверяет count и вывод сообщение
  kinoman: function () {
    if (this.count < 10) {
      alert("Просмотрено довольно мало фильмов");
    } else if (this.count >= 10 && this.count <= 30) {
      alert("Вы классический зритель");
    } else if (this.count > 30) {
      alert("Вы киноман");
    } else {
      alert("Произошла ошибка");
    }
  },

  // метод numberOfFilms задает пользователю вопрос
  // проверяет введенную строку методом limit
  // если пользователь накосячил с вводом, то
  // рекурсивно запускает саму себя, то есть снова задает пользователю вопрос и снова проверяет его
  // когда пользователь наконец сделает корректный ввод, то вызовется функция kinoman, а далее произойдет выход из рекурсии
  numberOfFilms: function () {
    let temp = prompt("Сколько фильмов вы уже посмотрели?", "");
    if (this.limit(temp)) {
      this.numberOfFilms();
    } else {
      this.count = temp;
      this.kinoman();
    }
  },

  // проверяется каждый ввод, если он некорректный, то итерация сбрасывается
  // цикл будет повторяться до тех пор пока пользователь не сделает 2(на самом деле 4) корректный ввода
  filmRating: function () {
    for (let i = 0; i < 2; i++) {
      let filmName = prompt("Один из последних просмотренных фильмов?", "");
      if (this.limit(filmName)) {
        i--;
        continue;
      }
      let filmRating = prompt("На сколько оцените его?", "");
      if (this.limit(filmRating)) {
        i--;
        continue;
      }
      this.movies[filmName] = filmRating;
    }
  },

  // проверяем свойство private
  showMyDB: function () {
    if (!this.private) {
      console.log(personalMovieDB);
    }
  },

  // после каждого ввода запускаем проверку через limit,
  //если ввод проверку не прошел, то сбрасывает итерацию
  writeYourGenres: function () {
    for (let i = 0; i <= 2; i++) {
      let temp = prompt(`Ваш любимый жанр под номером ${i + 1}`);
      if (this.limit(temp)) {
        i--;
        continue;
      }
      this.genres[i] = temp;
    }
		this.genres.forEach(function(item, i){
			console.log(`Любимый жанр #${i+1} - это ${item}`)
		})
  },

  // 2
  toggleVisibleMyDB: function () {
    // я не понимаю зачем здесь нужна проверка, ведь мы в любом случае даем private противоположное значение
    this.private = !this.private;

		// реализация с проверкой, где мы явно указываем значение private
    // if(this.private){
    // 	this.private = false;
    // }else{
    // 	this.private = true;
    // }
  },
};

// можем заполнить personalMovieDB значениями, но в данном примере это не нужно,
// тк мы задали значения по умолчанию
// personalMovieDB.numberOfFilms();
// personalMovieDB.filmRating();
// personalMovieDB.writeYourGenres();

// добавляем на страницу объекты со значениями их personalMovieDB

let numberOfFilms = document.createElement('div');
numberOfFilms.id = "number-of-films";
numberOfFilms.innerHTML = `Вы посмотрели вот столько фильмов: ${personalMovieDB.count}`;
document.body.append(numberOfFilms);

for(let movie in personalMovieDB.movies){
let anotherMovie = document.createElement('div');
anotherMovie.classList.add('movie')
anotherMovie.innerHTML = `Фильм- ${movie} Оценка - ${personalMovieDB.movies[movie]}`;
//document.body.append(anotherMovie);
document.body.appendChild(anotherMovie);
}

personalMovieDB.genres.forEach(function(item, i){
let anotherGenre = document.createElement('div');
anotherGenre.className = "genre";
anotherGenre.innerHTML = `Любимый жанр <strong>#${i+1}</strong> - это <button>${item}</button>`;
document.body.append(anotherGenre);
})

// забываем о том, что мы и так имеем на руках все эелементы страницы и получаем их заново
const number_of_films = document.getElementById('number-of-films'),
			btns = document.getElementsByTagName('button'),
			movies = document.getElementsByClassName('movie'),
			genres = document.getElementsByClassName('genre'),
			numbers = document.querySelectorAll('strong'),
			oneMovie = document.querySelector('.movie')

// методы, работающие похожим образом закомментированы


//  document.body.prepend(movies[0]);

btns[0].textContent = " ЭТО КНОПКА"

//number_of_films.after(movies[1]);
number_of_films.before(movies[0]);
//document.body.insertBefore(number_of_films, movies[1])

genres[1].remove()
// document.body.removeChild(genres[1])

genres[1].replaceWith(movies[1])
// document.body.replaceChild(genres[1], movies[0])

movies[0].insertAdjacentHTML("beforebegin", '<h1>Hello World!</h1>'	)